import React from 'react'
import './App.css'
import Header from "./Header";
import MemeGenerator from "../components/MemeGenerator";

const App = () => {
  return (
    <div className="App">
      <Header />
      <MemeGenerator />
    </div>
  )
}

export default App
