import React from 'react'

const Meme = ({topText, bottomText, picture, altText}) => {
    return (
        <div className='meme'>
            <h2 className='top'>{topText}</h2>
            <h2 className='bottom'>{bottomText}</h2>
            <img src={picture} alt={altText} />
        </div>
    )
}

export default Meme
