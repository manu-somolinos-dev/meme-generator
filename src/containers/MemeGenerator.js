import React from 'react'
import Meme from "../components/Meme";
import {connect} from "react-redux";
import {requestMemes,setMemeText,generateMeme} from "../actions";

const mapStateToProps = (state) => {
    return {
        memes: state.requestMemes.memes,
        isPending: state.requestMemes.isPending,
        topText: state.setMemeText.topText,
        bottomText: state.setMemeText.bottomText,
        picture: state.generateMeme.picture,
        altText: state.generateMeme.altText
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onGenerateMeme: () => {
            const randomPicture = this.props.memes[Math.floor(Math.random() * this.props.memes.length)]
            dispatch(generateMeme(randomPicture.url, randomPicture.name))
        },
        onChangeInput: (event) => dispatch(setMemeText(event.target.name, event.target.value)),
        onRequestMemes: () => dispatch(requestMemes())
    }
}


class MemeGenerator extends React.Component {

    componentDidMount() {
        this.props.onRequestMemes()
    }

    render() {
        const {onChangeInput, onGenerateMeme} = this.props;
        return (
            <main>
                <form className='meme-form'>
                    <input
                        placeholder='Top Text'
                        type='text'
                        name='topText'
                        value={this.props.topText}
                        onChange={onChangeInput} />
                    <input
                        placeholder='Bottom Text'
                        type='text'
                        name='bottomText'
                        value={this.props.bottomText}
                        onChange={onChangeInput} />
                    <button type='button' onClick={onGenerateMeme}>Generate</button>
                </form>
                <Meme topText={this.props.topText}
                      bottomText={this.props.bottomText}
                      picture={this.props.picture}
                      altText={this.props.altText} />
            </main>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MemeGenerator)
