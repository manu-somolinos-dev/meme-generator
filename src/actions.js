import {
    GENERATE_MEME,
    REQUEST_MEMES_FAILURE,
    REQUEST_MEMES_PENDING,
    REQUEST_MEMES_SUCCESS,
    SET_MEME_TEXT
} from "./constants";

export const requestMemes = () => (dispatch) => {
    dispatch({ type: REQUEST_MEMES_PENDING })
    fetch('https://api.imgflip.com/get_memes')
        .then(response => response.json())
        .then(data => {
            dispatch({
                type: REQUEST_MEMES_SUCCESS,
                payload: data.data.memes
            })
            dispatch({
                type: GENERATE_MEME,
                payload: {
                    picture: data.data.memes[0].url,
                    altText: data.data.memes[0].altText
                }
            })
        })
        .catch(error => dispatch({
            type: REQUEST_MEMES_FAILURE,
            payload: error
        }))
}

export const setMemeText = (field, value) => ({
    type: SET_MEME_TEXT,
    payload: {
        field: field,
        value: value
    }
})

export const generateMeme = (picture, altText) => ({
    type: GENERATE_MEME,
    payload: {
        picture: picture,
        altText: altText
    }
})
