import {
    GENERATE_MEME,
    REQUEST_MEMES_FAILURE,
    REQUEST_MEMES_PENDING,
    REQUEST_MEMES_SUCCESS,
    SET_MEME_TEXT
} from "./constants";

const initialState = {
    isPending: false,
    memes: [],
    error: null
}

export const requestMemes = (state = initialState, action = {}) => {
    switch (action.type) {
        case REQUEST_MEMES_PENDING:
            return {...state, isPending: true, error: null}
        case REQUEST_MEMES_SUCCESS:
            return {...state, isPending: false, memes: action.payload}
        case REQUEST_MEMES_FAILURE:
            return {...state, isPending: false, error: action.payload}
        default:
            return state
    }
}

const initialFormState = {
    topText: '',
    bottomText: ''
}

export const setMemeText = (state = initialFormState, action = {}) => {
    switch (action.type) {
        case SET_MEME_TEXT:
            return {...state, [action.payload.field]: action.payload.value}
        default:
            return state
    }
}

const initialMemeState = {
    picture: '',
    altText: ''
}

export const generateMeme = (state = initialMemeState, action = {}) => {
    switch (action.type) {
        case GENERATE_MEME:
            return {...state, picture: action.payload.picture, altText: action.payload.altText}
        default:
            return state
    }
}
