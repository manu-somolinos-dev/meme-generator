import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import * as serviceWorker from './serviceWorker'
import thunkMiddleware from 'redux-thunk'
import {createLogger} from "redux-logger"
import {requestMemes, setMemeText, generateMeme} from "./reducers"
import {applyMiddleware, combineReducers, createStore} from "redux"
import {Provider} from "react-redux"
import MemeGenerator from "./containers/MemeGenerator";

const logger = createLogger()

const reducer = combineReducers({setMemeText, requestMemes, generateMeme})
const store = createStore(reducer, applyMiddleware(thunkMiddleware, logger))

ReactDOM.render(
    <Provider store={store}>
        <MemeGenerator />
    </Provider>, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
